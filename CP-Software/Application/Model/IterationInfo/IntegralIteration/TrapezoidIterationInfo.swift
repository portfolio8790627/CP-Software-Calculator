//
//  TrapezoidIterationInfo.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 10.04.2023.
//

struct TrapezoidIterationInfo: IterationInfo {
    let iteration: Int
    let x: Double
    let functionValue: Double
}
