//
//  LeftRectanglesInfo.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 26.03.2023.
//

struct LeftRectanglesInfo: IterationInfo {
    let iteration: Int
    let x: Double
    let functionValue: Double
}
