//
//  SimpsonIterationInfo.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 10.04.2023.
//

import Foundation

struct SimpsonIterationInfo: IterationInfo {
    let iteration: Int
    let x1, x2, x3: Double
}
