//
//  MathProblems.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 08.04.2023.
//

import SwiftUI

struct MathProblems: View {
    
    //MARK: Properties
    
    @State private var selectedMathProblem: MathProblemType?
    
    //MARK: - Body
    
    var body: some View {
        NavigationStack {
            List {
                ForEach(MathProblemType.allCases) { mathProblem in
                    Section {
                        mathProblemRow(ofType: mathProblem)
                    }
                }
            }
            .navigationTitle("Math Problems")
        }
        .tint(.pink)
        .fullScreenCover(item: $selectedMathProblem) { mathProblem in
            switch mathProblem {
            case .nonlinearEquations:
                EquationInput<EquationInputViewModelImpl>()
            case .definiteIntegrals:
                DefiniteIntegralInput<DefiniteIntegralInputViewModelImpl>()
            case .functionInterpolation:
                FunctionInterpolation<FunctionInterpolationViewModelImpl>()
            }
        }
    }
}

//MARK: - Local Views

private extension MathProblems {
    func mathProblemRow(ofType problemType: MathProblemType) -> some View {
        HStack(spacing: 10.0) {
            VStack(alignment: .leading, spacing: 10.0) {
                Text(problemType.title)
                    .font(.system(.title2, weight: .semibold))
                Text(problemType.description)
                    .font(.subheadline)
                    .foregroundColor(.secondary)
            }
            Spacer()
            circleArrowButton(for: problemType)
        }
        .padding(8)
    }
    
    func circleArrowButton(for mathProblemType: MathProblemType) -> some View {
        Button {
            selectedMathProblem = mathProblemType
        } label: {
            Image(systemName: "arrow.up")
                .fontWeight(.semibold)
                .foregroundColor(.white)
                .padding(10)
                .background {
                    Circle().fill(.pink)
                }
        }
    }
}

//MARK: - Preview

struct MathProblems_Previews: PreviewProvider {
    static var previews: some View {
        MathProblems()
    }
}
