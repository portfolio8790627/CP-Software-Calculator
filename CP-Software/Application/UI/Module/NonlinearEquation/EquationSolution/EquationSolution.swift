//
//  EquationSolution.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 24.02.2023.
//

import SwiftUI

struct EquationSolution<ViewModel: EquationSolutionViewModel>: View {
    
    //MARK: Properties
    
    @StateObject private var viewModel: ViewModel
    @State private var solveTask: Task<Void, Never>?
    private let equation: String
    
    //MARK: - Initialization
    
    init(equation: String,
         equationExpression: NSExpression,
         solvingMethod: EquationSolvingMethod) {
        self.equation = equation
        _viewModel = StateObject(
            wrappedValue: EquationSolutionViewModelImpl(
                expression: equationExpression,
                solvingMethod: solvingMethod
            ) as! ViewModel)
    }
    
    //MARK: - Body
    
    var body: some View {
        List {
            Section {
                EquationSolutionParametersRepresentation<ViewModel>(equation: equation)
                    .environmentObject(viewModel)
            } footer: {
                if viewModel.iterationsInfo.endIndex > 0 {
                    VStack(alignment: .leading) {
                        if let answer = viewModel.answer {
                            EquationSolutionAnswer(
                                answer: answer,
                                functionValue: viewModel.functionValue(answer)
                            )
                        } else {
                            AnswerText(text: "No solution")
                        }
                    }
                    .padding(.top)
                }
            }
            
            if !viewModel.iterationsInfo.isEmpty {
                Section("Iterations") {
                    EquationSolvingSteps(
                        solvingMethod: viewModel.solvingMethod,
                        iterationsInfo: viewModel.iterationsInfo
                    )
                }
            }
        }
        .onTapGesture {
            endEditing()
        }
        .navigationBarTitleDisplayMode(.large)
        .navigationTitle(viewModel.solvingMethodTitle)
        .overlay {
            VStack {
                Spacer()
                ZStack {
                    if viewModel.isSolveButtonEnable {
                        SolveButton {
                            endEditing()
                            
                            solveTask = Task {
                                await viewModel.solve()
                            }
                        }
                        .transition(.opacity.animation(.easeIn(duration: 0.2)))
                        .padding()
                    }
                }
            }
        }
        .onDisappear {
            solveTask?.cancel()
        }
    }
}

//MARK: - Preview

struct EquationSolution_Previews: PreviewProvider {
    static let equation = "X**4-2*X-4"
    static var previews: some View {
        EquationSolution<EquationSolutionViewModelImpl>(
            equation: "X^(4)-2×X-4",
            equationExpression: equation.expression,
            solvingMethod: .dichotomy
        )
    }
}
