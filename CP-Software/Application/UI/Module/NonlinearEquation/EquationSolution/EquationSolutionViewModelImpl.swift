//
//  EquationSolutionViewModelImpl.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 24.02.2023.
//

import Foundation
import Combine

final class EquationSolutionViewModelImpl: EquationSolutionViewModel {
    
    //MARK: Properties
    
    let expression: NSExpression
    
    var solvingMethodTitle: String {
        solvingMethod.rawValue
    }
    
    var arePreviousValuesNotEntered: Bool {
        guard let solvingService else { return true }
        
        return lowerBound.value != solvingService.range.lowerBound ||
                upperBound.value != solvingService.range.upperBound ||
                epsilon.value != solvingService.epsilon
    }
    
    var isSolveButtonEnable: Bool {
        checkBounds() &&
        (epsilon.value ?? 0).isGreaterThanZero &&
        arePreviousValuesNotEntered
    }
    
    let lowerBoundInput: CurrentValueSubject<String, Never> = .init("")
    let upperBoundInput: CurrentValueSubject<String, Never> = .init("")
    let epsilonInput: CurrentValueSubject<String, Never> = .init("")
    
    @MainActor
    @Published var answer: Double?
    
    @MainActor
    @Published var iterationsInfo: [IterationInfo] = []
    
    let solvingMethod: EquationSolvingMethod
    
    private let lowerBound: CurrentValueSubject<Double?, Never> = .init(nil)
    private let upperBound: CurrentValueSubject<Double?, Never> = .init(nil)
    private let epsilon: CurrentValueSubject<Double?, Never> = .init(nil)
    
    private var solvingService: EquationSolvingService?
    
    private var cancellables = Set<AnyCancellable>()
    
    //MARK: - Initialization
    
    init(expression: NSExpression, solvingMethod: EquationSolvingMethod) {
        self.expression = expression
        self.solvingMethod = solvingMethod
        makeBindings()
    }
    
    //MARK: - Methods
    
    func solve() async {
        guard
            let lowerBoundValue = lowerBound.value,
            let upperBoundValue = upperBound.value,
            let epsilonValue = epsilon.value
        else { return }
                
        solvingService = EquationSolvingServiceImpl(
            expression: expression,
            range: lowerBoundValue...upperBoundValue,
            epsilon: epsilonValue
        )
        
        if let result = await solvingService?.solve(by: solvingMethod) {
            await MainActor.run {
                answer = result.answer
                iterationsInfo = result.iterationsInfo
            }
        }
    }
    
    func functionValue(_ x: Double) -> Double {
        solvingService?.expressionFunction(x) ?? 0
    }
}

//MARK: - Private methods

private extension EquationSolutionViewModelImpl {
    func makeBindings() {
        lowerBoundInput
            .mapToNumberAndSend(
                numberType: Double.self,
                to: lowerBound,
                stroreIn: &cancellables
            )
        
        upperBoundInput
            .mapToNumberAndSend(
                numberType: Double.self,
                to: upperBound,
                stroreIn: &cancellables
            )
        
        epsilonInput
            .mapToNumberAndSend(
                numberType: Double.self,
                to: epsilon,
                stroreIn: &cancellables
            )
        
        lowerBound.combineLatest(upperBound, epsilon)
            .sink { [weak self] _ in self?.objectWillChange.send() }
            .store(in: &cancellables)
    }
    
    func checkBounds() -> Bool {
        guard
            let lowerBoundValue = lowerBound.value,
            let upperBoundValue = upperBound.value,
            lowerBoundValue < upperBoundValue
        else { return false }
        return solvingMethod == .dichotomy ? abs(lowerBoundValue) - abs(upperBoundValue) != 0 : true
    }
}
