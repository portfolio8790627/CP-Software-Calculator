//
//  EquationInput.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 16.02.2023.
//

import SwiftUI

struct EquationInput<ViewModel: EquationInputViewModel>: View {
    
    //MARK: Properties
    
    @Environment(\.dismiss) private var dismiss
    @StateObject private var viewModel: ViewModel
    @State private var isConfirmationPresented = false
    
    //MARK: - Initialization
    
    init() {
        _viewModel = StateObject(
            wrappedValue: EquationInputViewModelImpl() as! ViewModel
        )
    }
    
    //MARK: - Body
    
    var body: some View {
        NavigationStack {
            Calculator(
                viewModel: viewModel,
                isConfirmationPresented: $isConfirmationPresented,
                expressionInput: {
                    equationInputText
                }, solutionButton: {
                    solutionButton
                }
            )
            .navigationDestination(for: EquationSolvingMethod.self) { solvingMethod in
                EquationSolution<EquationSolutionViewModelImpl>(
                    equation: viewModel.expressionString,
                    equationExpression: viewModel.expression,
                    solvingMethod: solvingMethod)
            }
            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    CloseButton {
                        dismiss.callAsFunction()
                    }
                }
            }
        }
        .tint(.pink)
    }
}

//MARK: - Local Views

private extension EquationInput {
    var equationInputText: some View {
        ScrollableExpression(viewModel: viewModel) {
            HStack(spacing: 0) {
                Text("F(x) = ")
                Text(viewModel.isExpressionEmpty ? "Input equation..." : viewModel.expressionString)
                    .foregroundColor(viewModel.isExpressionEmpty ? .secondary : .primary)
            }
            .font(.system(.title, design: .rounded, weight: .medium))
        }
    }
    
    var solutionButton: some View {
        ShowSolutionButton(viewModel: viewModel) {
            isConfirmationPresented.toggle()
        }
        .confirmationDialog("Solution methods", isPresented: $isConfirmationPresented, actions: {
            ForEach(viewModel.solvingMethods, id: \.self) { solutionMehod in
                NavigationLink(solutionMehod.rawValue, value: solutionMehod)
            }
        }, message: {
            Text("Choose a method for solving the equation")
        })
    }
}

//MARK: - Preview

struct EquationInput_Previews: PreviewProvider {
    static var previews: some View {
        EquationInput<EquationInputViewModelImpl>()
    }
}
