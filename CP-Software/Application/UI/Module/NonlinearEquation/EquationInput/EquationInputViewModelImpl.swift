//
//  EquationInputViewModelImpl.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 19.02.2023.
//

import Foundation
import Combine

final class EquationInputViewModelImpl: EquationInputViewModel {
    
    //MARK: Properties
    
    @Published var expressionString = String()
    
    var currentKeyboardSection: CurrentValueSubject<MathKeyboardSection, Never> = .init(.numbersAndGeneralOperations)
    
    let solvingMethods = EquationSolvingMethod.allCases
    
    var isReadyToSolve: Bool {
        !isExpressionEmpty &&
        inputManager.areBracketsBalanced &&
        expressionString.contains(AuxiliaryMathOperation.x.title) &&
        !inputManager.isLastBasicOperation &&
        !inputManager.isLastDot
    }
    
    var expression: NSExpression {
        inputManager.equationExpressionComponents.joined().expression
    }
    
    var isExpressionEmpty: Bool {
        inputManager.isEquationEmpty
    }
    
    private let inputManager: ExpressionInputManager
    private var cancellables = Set<AnyCancellable>()
    
    //MARK: - Initialization
    
    init(inputManager: ExpressionInputManager = ExpressionInputManagerImpl()) {
        self.inputManager = inputManager
        
        inputManager.equationSubject
            .assign(to: &$expressionString)
        
        currentKeyboardSection
            .sink { [weak self] _ in self?.objectWillChange.send() }
            .store(in: &cancellables)
    }
    
    //MARK: - Methods
    
    func addNumber(_ number: Int) {
        inputManager.addNumber(number)
    }
    
    func addBasicOperation(_ operation: BasicMathOperation) {
        inputManager.addBasicOperation(operation)
    }
    
    func addAdvancedOperation(_ operation: AdvancedMathOperation) {
        inputManager.addAdvancedOperation(operation)
    }
    
    func addAuxiliaryOperation(_ operation: AuxiliaryMathOperation) {
        inputManager.addAuxiliaryOperation(operation)
    }

    func removeLastOperation() {
        inputManager.removeLastOperation()
    }
    
    func clearAll() {
        inputManager.clearAll()
    }
}
