//
//  DefiniteIntegralInput.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 08.04.2023.
//

import SwiftUI

struct DefiniteIntegralInput<ViewModel: DefiniteIntegralInputViewModel>: View {
    
    //MARK: Properties
    
    @Environment(\.dismiss) private var dismiss
    @StateObject private var viewModel: ViewModel
    @State private var isConfirmationPresented = false
    
    //MARK: - Initialization
    
    init() {
        _viewModel = StateObject(
            wrappedValue: DefiniteIntegralInputViewModelImpl() as! ViewModel
        )
    }
    
    //MARK: - Body
    
    var body: some View {
        NavigationStack {
            Calculator(
                viewModel: viewModel,
                isConfirmationPresented: $isConfirmationPresented,
                expressionInput: {
                    equationInputText
                },
                solutionButton: {
                    solutionButton
                }
            )
            .navigationDestination(for: DefiniteIntegralSolvingMethod.self) { solvingMethod in
                DefiniteIntegralSolution<DefiniteIntegralSolutionViewModelImpl>(
                    expressionString: viewModel.expressionString,
                    expression: viewModel.expression,
                    solvingMethod: solvingMethod
                )
            }
            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    CloseButton {
                        dismiss.callAsFunction()
                    }
                }
            }
        }
        .tint(.pink)
    }
}

//MARK: - Local Views

private extension DefiniteIntegralInput {
    var equationInputText: some View {
        ScrollableExpression(viewModel: viewModel) {
            DefiniteIntegralView(expressionString: viewModel.expressionString)
                .font(.system(.title, design: .rounded, weight: .medium))
        }
    }
    
    var solutionButton: some View {
        ShowSolutionButton(viewModel: viewModel) {
            isConfirmationPresented.toggle()
        }
        .confirmationDialog("Solution methods", isPresented: $isConfirmationPresented, actions: {
            ForEach(viewModel.solvingMethods, id: \.self) { solvingMethod in
                NavigationLink(solvingMethod.rawValue, value: solvingMethod)
            }
        }, message: {
            Text("Choose a method for solving the integral")
        })
    }
}

//MARK: - Preview

struct DefiniteIntegralInput_Previews: PreviewProvider {
    static var previews: some View {
        DefiniteIntegralInput<DefiniteIntegralInputViewModelImpl>()
    }
}
