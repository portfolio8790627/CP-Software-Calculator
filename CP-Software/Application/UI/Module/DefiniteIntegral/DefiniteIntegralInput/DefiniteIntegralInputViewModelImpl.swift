//
//  DefiniteIntegralInputViewModelImpl.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 08.04.2023.
//

import Foundation
import Combine

final class DefiniteIntegralInputViewModelImpl: DefiniteIntegralInputViewModel {
    
    //MARK: Properties
    
    let solvingMethods: [DefiniteIntegralSolvingMethod] = DefiniteIntegralSolvingMethod.allCases
    
    @Published var expressionString: String = String()
    
    var expression: NSExpression {
        inputManager.equationExpressionComponents.joined().expression
    }
    
    var isExpressionEmpty: Bool {
        inputManager.isEquationEmpty
    }
    
    var isReadyToSolve: Bool {
        !isExpressionEmpty &&
        inputManager.areBracketsBalanced &&
        expressionString.contains(AuxiliaryMathOperation.x.title) &&
        !inputManager.isLastBasicOperation &&
        !inputManager.isLastDot
    }
    
    var currentKeyboardSection: CurrentValueSubject<MathKeyboardSection, Never> = .init(.numbersAndGeneralOperations)
    
    private let inputManager: ExpressionInputManager
    private var cancellables = Set<AnyCancellable>()
    
    //MARK: - Initialization
    
    init(inputManager: ExpressionInputManager = ExpressionInputManagerImpl()) {
        self.inputManager = inputManager
        makeBindings()
    }
    
    //MARK: - Methods
    
    func addNumber(_ number: Int) {
        inputManager.addNumber(number)
    }
    
    func addBasicOperation(_ operation: BasicMathOperation) {
        inputManager.addBasicOperation(operation)
    }
    
    func addAdvancedOperation(_ operation: AdvancedMathOperation) {
        inputManager.addAdvancedOperation(operation)
    }
    
    func addAuxiliaryOperation(_ operation: AuxiliaryMathOperation) {
        inputManager.addAuxiliaryOperation(operation)
    }

    func removeLastOperation() {
        inputManager.removeLastOperation()
    }
    
    func clearAll() {
        inputManager.clearAll()
    }
}

//MARK: - Private methods

private extension DefiniteIntegralInputViewModelImpl {
    func makeBindings() {
        inputManager.equationSubject
            .assign(to: &$expressionString)
        
        currentKeyboardSection
            .sink { [weak self] _ in self?.objectWillChange.send() }
            .store(in: &cancellables)
    }
}
