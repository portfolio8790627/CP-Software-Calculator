//
//  DefiniteIntegralSolutionViewModelImpl.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 08.04.2023.
//

import Foundation
import Combine

final class DefiniteIntegralSolutionViewModelImpl: DefiniteIntegralSolutionViewModel {
    
    //MARK: Properties
    
    let expression: NSExpression
    
    var solvingMethodTitle: String {
        solvingMethod.rawValue
    }
    
    var arePreviousValuesNotEntered: Bool {
        guard let solvingService else { return true }
        
        return integralLowerBound.value != solvingService.range.lowerBound ||
                integralUpperBound.value != solvingService.range.upperBound ||
                partitionsCount.value != solvingService.partitionsCount ||
                epsilon.value != solvingService.epsilon
    }
    
    var isSolveButtonEnable: Bool {
        checkBounds() &&
        (epsilon.value ?? 0).isGreaterThanZero &&
        (partitionsCount.value ?? 0) > 1 &&
        arePreviousValuesNotEntered
    }
    
    let integralLowerBoundInput: CurrentValueSubject<String, Never> = .init("")
    let integralUpperBoundInput: CurrentValueSubject<String, Never> = .init("")
    let partitionsCountInput: CurrentValueSubject<String, Never> = .init("")
    let epsilonInput: CurrentValueSubject<String, Never> = .init("")
    
    @MainActor
    @Published var answer: Double?
    
    @MainActor
    @Published var iterationsInfo: [IterationInfo] = []
    
    let solvingMethod: DefiniteIntegralSolvingMethod
    
    private let integralLowerBound: CurrentValueSubject<Double?, Never> = .init(nil)
    private let integralUpperBound: CurrentValueSubject<Double?, Never> = .init(nil)
    private let partitionsCount: CurrentValueSubject<Int?, Never> = .init(nil)
    private let epsilon: CurrentValueSubject<Double?, Never> = .init(nil)
    
    private var solvingService: DefiniteIntegralSolvingService?
    private var cancellables = Set<AnyCancellable>()
    
    //MARK: - Initialization
    
    init(expression: NSExpression, solvingMethod: DefiniteIntegralSolvingMethod) {
        self.expression = expression
        self.solvingMethod = solvingMethod
        makeBindings()
    }
    
    //MARK: - Methods
    
    func solve() async {
        guard
            let lowerBound = integralLowerBound.value,
            let upperBound = integralUpperBound.value,
            let partitions = partitionsCount.value,
            let epsilonValue = epsilon.value
        else { return }
                
        solvingService = DefiniteIntegralSolvingServiceImpl(
            expression: expression,
            range: lowerBound...upperBound,
            partitionsCount: partitions,
            epsilon: epsilonValue
        )
        
        if let result = await solvingService?.solve(by: solvingMethod) {
            await MainActor.run {
                answer = result.answer
                iterationsInfo = result.iterationsInfo
            }
        }
    }
}

//MARK: - Private methods

private extension DefiniteIntegralSolutionViewModelImpl {
    func makeBindings() {
        integralLowerBoundInput
            .mapToNumberAndSend(
                numberType: Double.self,
                to: integralLowerBound,
                stroreIn: &cancellables
            )
        
        integralUpperBoundInput
            .mapToNumberAndSend(
                numberType: Double.self,
                to: integralUpperBound,
                stroreIn: &cancellables
            )
        
        partitionsCountInput
            .mapToNumberAndSend(
                numberType: Int.self,
                to: partitionsCount,
                stroreIn: &cancellables
            )
        
        epsilonInput
            .mapToNumberAndSend(
                numberType: Double.self,
                to: epsilon,
                stroreIn: &cancellables
            )
        
        integralLowerBound.combineLatest(integralUpperBound, partitionsCount, epsilon)
            .sink { [weak self] _ in self?.objectWillChange.send() }
            .store(in: &cancellables)
    }
    
    func checkBounds() -> Bool {
        guard
            let lowerBound = integralLowerBound.value,
            let upperBound = integralUpperBound.value
        else { return false }
        return lowerBound < upperBound
    }
}
