//
//  DefiniteIntegralSolution.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 08.04.2023.
//

import SwiftUI

struct DefiniteIntegralSolution<ViewModel: DefiniteIntegralSolutionViewModel>: View {
    
    //MARK: Properties
    
    @StateObject private var viewModel: ViewModel
    @State private var solveTask: Task<Void, Never>?
    let expressionString: String
    
    //MARK: - Initialization
    
    init(expressionString: String, expression: NSExpression, solvingMethod: DefiniteIntegralSolvingMethod) {
        self.expressionString = expressionString
        _viewModel = StateObject(
            wrappedValue: DefiniteIntegralSolutionViewModelImpl(
                expression: expression,
                solvingMethod: solvingMethod
            ) as! ViewModel
        )
    }
    
    //MARK: - Body
    
    var body: some View {
        List {
            Section {
                DefiniteIntegralSolutionParametersRepresentation<ViewModel>(expressionString: expressionString)
                    .environmentObject(viewModel)
            } footer: {
                if let answer = viewModel.answer {
                    DefiniteIntegralSolutionAnswer(answer: answer)
                        .padding(.top)
                }
            }
            
            if !viewModel.iterationsInfo.isEmpty {
                Section("Iterations") {
                    DefiniteIntegralSolvingSteps(
                        solvingMethod: viewModel.solvingMethod,
                        iterationsInfo: viewModel.iterationsInfo
                    )
                }
            }
        }
        .navigationTitle(viewModel.solvingMethodTitle)
        .navigationBarTitleDisplayMode(.large)
        .overlay {
            VStack {
                Spacer()
                ZStack {
                    if viewModel.isSolveButtonEnable {
                        SolveButton {
                            endEditing()
                            solveTask = Task {
                                await viewModel.solve()
                            }
                        }
                        .transition(.opacity.animation(.easeIn(duration: 0.2)))
                        .padding()
                    }
                }
            }
        }
        .onDisappear {
            solveTask?.cancel()
        }
    }
}

struct DefiniteIntegralSolution_Previews: PreviewProvider {
    static var previews: some View {
        NavigationStack {
            DefiniteIntegralSolution<DefiniteIntegralSolutionViewModelImpl>(
                expressionString: "sqrt(2*x+1)",
                expression: "sqrt(2*x+1)".expression,
                solvingMethod: .leftRectangles)
        }
    }
}
