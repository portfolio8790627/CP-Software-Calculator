//
//  FunctionInterpolation.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 11.04.2023.
//

import SwiftUI

struct FunctionInterpolation<ViewModel: FunctionInterpolationSolutionViewModel>: View {
    
    //MARK: Properties
    
    @Environment(\.dismiss) private var dismiss
    
    @StateObject private var viewModel: ViewModel
    
    @State private var solvingTask: Task<Void, Never>?
        
    @State private var firstXText = String()
    @State private var secondXText = String()
    @State private var firstFunctionValueText = String()
    @State private var secondFunctionValueText = String()
    @State private var targetXText = String()
    
    //MARK: - Initialization
    
    init() {
        _viewModel = StateObject(
            wrappedValue: FunctionInterpolationViewModelImpl() as! ViewModel
        )
    }
    
    //MARK: - Body
    
    var body: some View {
        NavigationStack {
            List {
                Section {
                    VStack(alignment: .leading) {
                        initialValuesInput
                        
                        HStack {
                            Text("Target value X = ")
                                .foregroundColor(.pink)
                            ValueInputTextField(title: "X", text: $targetXText)
                        }
                        .font(.system(.title3, weight: .semibold))
                        .padding()
                    }
                } footer: {
                    if let answer = viewModel.answer {
                        VStack(alignment: .leading) {
                            AnswerText(text: "Answer:")
                            AnswerText(text: String(answer))
                        }
                        .padding(.vertical)
                    }
                }
            }
            .navigationTitle("Linear Interpolation")
            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    CloseButton {
                        dismiss.callAsFunction()
                    }
                }
            }
        }
        .tint(.pink)
        .overlay {
            VStack {
                Spacer()
                
                if viewModel.isSolveButtonEnable {
                    SolveButton {
                        endEditing()
                        
                        solvingTask = Task {
                            await viewModel.solve()
                        }
                    }
                    .transition(.opacity.animation(.easeIn(duration: 0.2)))
                    .padding()
                }
            }
        }
        .onChange(of: firstXText, perform: viewModel.firstXInput.send)
        .onChange(of: secondXText, perform: viewModel.secondXInput.send)
        .onChange(of: firstFunctionValueText, perform: viewModel.firstFunctionValueInput.send)
        .onChange(of: secondFunctionValueText, perform: viewModel.secondFunctionValueInput.send)
        .onChange(of: targetXText, perform: viewModel.targetXValueInput.send)
        .onDisappear {
            solvingTask?.cancel()
        }
    }
}

//MARK: - Local Views

private extension FunctionInterpolation {
    @ViewBuilder
    var initialValuesInput: some View {
        initialValueInput(
            index: 1,
            xText: $firstXText,
            functionValueText: $firstFunctionValueText
        )
        
        initialValueInput(index: 2, xText: $secondXText, functionValueText: $secondFunctionValueText)
    }
    
    func initialValueInput(index: Int, xText: Binding<String>, functionValueText: Binding<String>) -> some View {
        HStack(spacing: 25) {
            HStack {
                Text("X\(index)")
                ValueInputTextField(title: "X", text: xText)
            }
            
            HStack {
                Text("F(x)")
                ValueInputTextField(title: "F(x)", text: functionValueText)
            }
        }
        .font(.system(.headline, design: .rounded))
        .padding()
    }
}

//MARK: - Preview

struct FunctionInterpolation_Previews: PreviewProvider {
    static var previews: some View {
        FunctionInterpolation<FunctionInterpolationViewModelImpl>()
    }
}
