//
//  FunctionInterpolationViewModelImpl.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 11.04.2023.
//

import Combine

final class FunctionInterpolationViewModelImpl: FunctionInterpolationSolutionViewModel {
    
    //MARK: Properties
    
    let firstXInput: CurrentValueSubject<String, Never> = .init("")
    let secondXInput: CurrentValueSubject<String, Never> = .init("")
    
    let firstFunctionValueInput: CurrentValueSubject<String, Never> = .init("")
    let secondFunctionValueInput: CurrentValueSubject<String, Never> = .init("")
    
    let targetXValueInput: CurrentValueSubject<String, Never> = .init("")
    
    var arePreviousValuesNotEntered: Bool {
        guard
            let solvingService,
            solvingService.xValues.count == 2,
            solvingService.functionValues.count == 2
        else { return true }
        
        return firstX.value != solvingService.xValues.first ||
                secondX.value != solvingService.xValues.last ||
                firstFunctionValue.value != solvingService.functionValues.first ||
                secondFunctionValue.value != solvingService.functionValues.last ||
                targetX.value != solvingService.targetX
    }
    
    var isSolveButtonEnable: Bool {
        inputIsValid && xValuesAreNotEqual && arePreviousValuesNotEntered
    }
    
    @MainActor
    @Published var answer: Double?
    
    private let firstX: CurrentValueSubject<Double?, Never> = .init(nil)
    private let secondX: CurrentValueSubject<Double?, Never> = .init(nil)
    
    private let firstFunctionValue: CurrentValueSubject<Double?, Never> = .init(nil)
    private let secondFunctionValue: CurrentValueSubject<Double?, Never> = .init(nil)
    
    private let targetX: CurrentValueSubject<Double?, Never> = .init(nil)
        
    private var solvingService: FunctionInterpolationSolvingService?
    private var cancellables = Set<AnyCancellable>()
    
    //MARK: - Initialization
    
    init() {
        makeBindings()
    }
    
    //MARK: - Methods
    
    func solve() async {
        guard
            let firstXValue = firstX.value,
            let secondXValue = secondX.value,
            let firstXFunctionValue = firstFunctionValue.value,
            let secondXFunctionValue = secondFunctionValue.value,
            let targetXValue = targetX.value
        else { return }
        
        solvingService = FunctionInterpolationSolvingServiceImpl(
            xValues: [firstXValue, secondXValue],
            functionValues: [firstXFunctionValue, secondXFunctionValue],
            targetX: targetXValue
        )
        
        let result = await solvingService?.solve()
        
        await MainActor.run {
            answer = result
        }
    }
}

//MARK: - Private methods

private extension FunctionInterpolationViewModelImpl {
    func makeBindings() {
        firstXInput
            .mapToNumberAndSend(
                numberType: Double.self,
                to: firstX,
                stroreIn: &cancellables
            )
        
        secondXInput
            .mapToNumberAndSend(
                numberType: Double.self,
                to: secondX,
                stroreIn: &cancellables
            )
        
        firstFunctionValueInput
            .mapToNumberAndSend(
                numberType: Double.self,
                to: firstFunctionValue,
                stroreIn: &cancellables
            )
        
        secondFunctionValueInput
            .mapToNumberAndSend(
                numberType: Double.self,
                to: secondFunctionValue,
                stroreIn: &cancellables
            )
        
        targetXValueInput
            .mapInput(to: Double.self)
            .map { [weak self] in
                guard
                    $0 ?? 0 > self?.firstX.value ?? 0,
                    $0 ?? 0 < self?.secondX.value ?? 0
                else { return nil }
                return $0
            }
            .sink { [weak self] in self?.targetX.send($0) }
            .store(in: &cancellables)
        
        firstX.combineLatest(secondX, firstFunctionValue, secondFunctionValue)
            .combineLatest(targetX)
            .sink { [weak self] _ in self?.objectWillChange.send() }
            .store(in: &cancellables)
    }
    
    var inputIsValid: Bool {
        firstX.value != nil &&
        secondX.value != nil &&
        firstFunctionValue.value != nil &&
        secondFunctionValue.value != nil &&
        targetX.value != nil
    }
    
    var xValuesAreNotEqual: Bool {
        firstX.value != secondX.value
    }
}
