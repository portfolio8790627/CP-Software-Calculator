//
//  IterationText.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 26.03.2023.
//

import SwiftUI

struct IterationText: View {
    let number: Int
    
    var body: some View {
        Text("Iteration \(number)")
            .font(.title2)
            .fontWeight(.semibold)
            .underline()
    }
}

struct IterationText_Previews: PreviewProvider {
    static var previews: some View {
        IterationText(number: 1)
    }
}
