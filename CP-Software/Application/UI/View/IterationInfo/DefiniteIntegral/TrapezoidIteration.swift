//
//  TrapezoidIteration.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 10.04.2023.
//

import SwiftUI

struct TrapezoidIteration: View {
    let iterationInfo: TrapezoidIterationInfo

    var body: some View {
        VStack(alignment: .leading, spacing: 20) {
            IterationText(number: iterationInfo.iteration)
            
            VStack(alignment: .leading) {
                Text("X = \(Text("\(iterationInfo.x)").foregroundColor(.pink))")
                Text("F(x) = \(Text("\(iterationInfo.functionValue)").foregroundColor(.pink))")
            }
            .font(.headline)
        }
        .padding()
    }
}

struct TrapezoidIteration_Previews: PreviewProvider {
    static var previews: some View {
        TrapezoidIteration(iterationInfo: .init(iteration: 1, x: 0.5, functionValue: 2.05))
    }
}
