//
//  SimpsonIteration.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 10.04.2023.
//

import SwiftUI

struct SimpsonIteration: View {
    let iterationInfo: SimpsonIterationInfo
    
    var body: some View {
        VStack(alignment: .leading, spacing: 20.0) {
            IterationText(number: iterationInfo.iteration)
            
            VStack(alignment: .leading) {
                Text("X1 = \(Text(String(iterationInfo.x1)).foregroundColor(.pink))")
                Text("X2 = \(Text(String(iterationInfo.x2)).foregroundColor(.pink))")
                Text("X3 = \(Text(String(iterationInfo.x3)).foregroundColor(.pink))")
            }
        }
        .font(.headline)
        .padding()
    }
}

struct SimpsonIteration_Previews: PreviewProvider {
    static var previews: some View {
        SimpsonIteration(
            iterationInfo: .init(
                iteration: 1,
                x1: 1, x2: 2, x3: 3
            )
        )
    }
}
