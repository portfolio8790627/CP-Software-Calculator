//
//  LeftRecatnglesIteration.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 26.03.2023.
//

import SwiftUI

struct LeftRecatnglesIteration: View {
    let iterationInfo: LeftRectanglesInfo

    var body: some View {
        VStack(alignment: .leading, spacing: 20) {
            IterationText(number: iterationInfo.iteration)
            
            VStack(alignment: .leading) {
                Text("X = \(Text("\(iterationInfo.x)").foregroundColor(.pink))")
                Text("F(x) = \(Text("\(iterationInfo.functionValue)").foregroundColor(.pink))")
            }
            .font(.headline)
        }
        .padding()
    }
}

struct LeftRecatnglesIteration_Previews: PreviewProvider {
    static var previews: some View {
        LeftRecatnglesIteration(iterationInfo: .init(iteration: 1, x: 0, functionValue: 2))
    }
}
