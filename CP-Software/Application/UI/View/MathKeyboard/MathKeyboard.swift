//
//  MathKeyboard.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 16.02.2023.
//

import SwiftUI

struct MathKeyboard<ViewModel: ExpressionModifierViewModel>: View {
    
    //MARK: Properties
    
    @ObservedObject var viewModel: ViewModel
    
    //MARK: - Body
    
    var body: some View {
        GeometryReader { geometry in
            VStack {
                HStack {
                    Spacer()
                    deleteButton
                }
                
                HStack(spacing: 15) {
                    ForEach(MathKeyboardSection.allCases, id: \.self) { section in
                        MathKeyboardSectionButton(
                            viewModel: viewModel,
                            keyboardSection: section
                        )
                    }
                }
                .frame(height: geometry.size.height * 0.05)
                .padding(.vertical)
                .padding(.horizontal, 5)
                
                switch viewModel.currentKeyboardSection.value {
                case .numbersAndGeneralOperations:
                    numbersAndGeneralOperationsView
                case .trigonometry:
                    TriginometryPad(viewModel: viewModel)
                case .expAndLogarithms:
                    GeometryReader { proxy in
                        ExponentAndLogarithmsPad(viewModel: viewModel)
                            .frame(height: proxy.size.height / 4)
                    }
                }
            }
        }
    }
}

//MARK: - Local Views

private extension MathKeyboard {
    var deleteButton: some View {
        Button {
            viewModel.removeLastOperation()
        } label: {
            Image(systemName: "delete.left")
                .font(.largeTitle)
                .padding()
        }
        .tint(.primary)
        .disabled(viewModel.isExpressionEmpty)
    }
    
    var numbersAndGeneralOperationsView: some View {
        GeometryReader { proxy in
            HStack(alignment: .top, spacing: 2) {
                VStack(spacing: 2) {
                    numbersPadAdvancedOperationButtons
                    numbersPadAuxiliaryOperationButtons
                }
                
                NumbersPad(viewModel: viewModel)
                    .frame(width: proxy.size.width * 2 / 3)
                
                BasicMathOperationsPad(viewModel: viewModel)
            }
        }
    }
    
    @ViewBuilder
    var numbersPadAdvancedOperationButtons: some View {
        let advancedOperations: [AdvancedMathOperation] = [.pow, .sqrt]
        
        ForEach(advancedOperations, id: \.self) { advancedOperation in
            AdvancedMathOperationButton(
                mathOperation: advancedOperation,
                tapAction: viewModel.addAdvancedOperation
            )
        }
    }
    
    @ViewBuilder
    var numbersPadAuxiliaryOperationButtons: some View {
        let auxiliaryOperations: [AuxiliaryMathOperation] = [.roundBracket, .x]
        
        ForEach(auxiliaryOperations, id: \.self) { auxiliaryOperation in
            AuxiliaryMathOperationButton(
                mathOperation: auxiliaryOperation,
                tapAction: viewModel.addAuxiliaryOperation
            )
        }
    }
}

//MARK: - Preview

struct MathKeyboard_Previews: PreviewProvider {
    @StateObject static var viewModel = EquationInputViewModelImpl()
    
    static var previews: some View {
        VStack {
            Spacer()
            MathKeyboard(viewModel: viewModel)
                .frame(height: 550)
        }
        .padding(8)
    }
}
