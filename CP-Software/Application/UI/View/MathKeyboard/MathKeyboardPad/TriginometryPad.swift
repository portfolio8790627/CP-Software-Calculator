//
//  TriginometryPad.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 04.03.2023.
//

import SwiftUI

struct TriginometryPad<ViewModel: ExpressionModifierViewModel>: View {
    
    //MARK: Properties
    
    @ObservedObject var viewModel: ViewModel
    
    //MARK: - Body
    
    var body: some View {
        HStack(spacing: 2) {
            VStack(spacing: 2) {
                let sinOperations: [AdvancedMathOperation] = [
                    .sin, .asin, .sinh, .asinh
                ]
                
                trigonometricButtons(operations: sinOperations)
            }
            
            VStack(spacing: 2) {
                let cosOperations: [AdvancedMathOperation] = [
                    .cos, .acos, .cosh, .acosh
                ]
                
                trigonometricButtons(operations: cosOperations)
            }
            
            VStack(spacing: 2) {
                let tanOperations: [AdvancedMathOperation] = [
                    .tan, .atan, .tanh, .atanh
                ]
                
                trigonometricButtons(operations: tanOperations)
            }
            
            VStack(spacing: 2) {
                let cotOperations: [AdvancedMathOperation] = [
                    .cot, .acot, .coth, .acoth
                ]
                
                trigonometricButtons(operations: cotOperations)
            }

        }
    }
}

//MARK: - Local Views

private extension TriginometryPad {
    @ViewBuilder
    func trigonometricButtons(operations: [AdvancedMathOperation]) -> some View {
        ForEach(operations, id: \.self) { operation in
            AdvancedMathOperationButton(
                mathOperation: operation,
                tapAction: viewModel.addAdvancedOperation
            )
        }
    }
    
}

//MARK: - Preview

struct TriginometryPad_Previews: PreviewProvider {
    @StateObject static var viewModel = EquationInputViewModelImpl()
    
    static var previews: some View {
        TriginometryPad(viewModel: viewModel)
    }
}
