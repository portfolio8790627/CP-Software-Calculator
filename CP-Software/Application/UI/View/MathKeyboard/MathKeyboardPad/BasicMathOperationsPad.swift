//
//  BasicMathOperationsPad.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 16.02.2023.
//

import SwiftUI

struct BasicMathOperationsPad<ViewModel: ExpressionModifierViewModel>: View {
    
    //MARK: Properties
    
    @ObservedObject var viewModel: ViewModel
    
    private let basicMathOperations: [BasicMathOperation] = [
        .div, .mul, .sub, .add
    ]
    
    //MARK: - Body
    
    var body: some View {
        VStack(spacing: 2) {
            ForEach(basicMathOperations, id: \.self) { mathOperation in
                BasicMathOperationButton(mathOperation: mathOperation) { operation in
                    viewModel.addBasicOperation(operation)
                }
            }
        }
    }
}

//MARK: - Preview

struct BasicMathOperationsPad_Previews: PreviewProvider {
    @StateObject static var viewModel = EquationInputViewModelImpl()
    
    static var previews: some View {
        BasicMathOperationsPad(viewModel: viewModel)
    }
}
