//
//  ExponentAndLogarithmsPad.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 01.04.2023.
//

import SwiftUI

struct ExponentAndLogarithmsPad<ViewModel: ExpressionModifierViewModel>: View {
    
    //MARK: Properties
    
    @ObservedObject var viewModel: ViewModel
    
    private let mathOperations: [AdvancedMathOperation] = [
        .e, .exp, .log2, .lg, .ln
    ]
    
    //MARK: - Body
    
    var body: some View {
        HStack(alignment: .top, spacing: 2) {
            ForEach(mathOperations, id: \.self) { mathOperation in
                AdvancedMathOperationButton(
                    mathOperation: mathOperation,
                    tapAction: viewModel.addAdvancedOperation
                )
            }
        }
    }
}

//MARK: - Preview

struct ExponentAndLogarithmsPad_Previews: PreviewProvider {
    @StateObject static var viewModel = EquationInputViewModelImpl()

    static var previews: some View {
        ExponentAndLogarithmsPad(viewModel: viewModel)
    }
}
