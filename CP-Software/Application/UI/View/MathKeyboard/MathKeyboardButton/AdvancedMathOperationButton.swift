//
//  AdvancedMathOperationButton.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 16.02.2023.
//

import SwiftUI

struct AdvancedMathOperationButton: View {
    let mathOperation: AdvancedMathOperation
    let tapAction: ((AdvancedMathOperation) -> Void)
    
    var body: some View {
        Button {
            tapAction(mathOperation)
        } label: {
            ZStack {
                Rectangle()
                
                Text(mathOperation.title)
                    .font(.title2)
                    .fontWeight(.medium)
                    .foregroundColor(.primary)
            }
        }
        .tint(.secondary.opacity(0.3))
    }
}

struct AdvancedMathOperationButton_Previews: PreviewProvider {
    static var previews: some View {
        AdvancedMathOperationButton(mathOperation: .pow, tapAction: { _ in })
    }
}
