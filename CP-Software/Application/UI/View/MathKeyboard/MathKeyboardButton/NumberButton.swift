//
//  NumberButton.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 16.02.2023.
//

import SwiftUI

struct NumberButton: View {
    let number: Int
    let tapAction: (Int) -> Void

    var body: some View {
        Button {
            tapAction(number)
        } label: {
            ZStack {
                Rectangle()
                
                Text(String(number))
                    .font(.title)
                    .fontWeight(.medium)
                    .foregroundColor(.primary)
            }
        }
        .tint(Color(.secondarySystemBackground))
    }
}

struct NumberButton_Previews: PreviewProvider {
    static var previews: some View {
        NumberButton(number: 1, tapAction: { _ in })
    }
}
