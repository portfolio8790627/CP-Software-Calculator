//
//  BasicMathOperationButton.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 16.02.2023.
//

import SwiftUI

struct BasicMathOperationButton: View {
    let mathOperation: BasicMathOperation
    let tapAction: ((BasicMathOperation) -> Void)
    
    var body: some View {
        Button {
            tapAction(mathOperation)
        } label: {
            ZStack {
                Rectangle()
                
                Text(mathOperation.title)
                    .font(.title)
                    .fontWeight(.medium)
                    .foregroundColor(Color(.systemBackground))
            }
        }
        .tint(.primary)
    }
}

struct MathOperationButton_Previews: PreviewProvider {
    static var previews: some View {
        BasicMathOperationButton(mathOperation: .add, tapAction: { _ in })
    }
}
