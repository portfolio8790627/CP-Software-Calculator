//
//  MathKeyboardSectionButton.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 01.03.2023.
//

import SwiftUI

struct MathKeyboardSectionButton<ViewModel: ExpressionModifierViewModel>: View {
    
    //MARK: Properties
    
    @ObservedObject var viewModel: ViewModel
    let keyboardSection: MathKeyboardSection
    
    private var isCurrentSection: Bool {
        keyboardSection == viewModel.currentKeyboardSection.value
    }
    
    //MARK: - Body
    
    var body: some View {
        ZStack {
            if isCurrentSection {
                strokedCapsule()
            } else {
                strokedCapsule(fillColor: Color(.secondarySystemBackground))
            }
            
            buttonTitle
                .font(.headline)
                .foregroundColor(isCurrentSection ? Color(.systemBackground) : .primary)
                .padding(5)
                .padding(.horizontal)
            
        }
        .onTapGesture {
            withAnimation(.easeIn(duration: 0.1)) {
                viewModel.currentKeyboardSection.send(keyboardSection)
            }
        }
    }
}

//MARK: - Local Views

private extension MathKeyboardSectionButton {
    var buttonTitle: some View {
        switch keyboardSection {
        case .numbersAndGeneralOperations:
            return VStack {
                Text("+  -")
                Text("×  ÷")
            }
        case .trigonometry:
            return VStack {
                Text("sin  cos")
                Text("tan  cot")
            }
        case .expAndLogarithms:
            return VStack {
                Text("e")
                Text("log  ln")
            }
        }
    }
    
    func strokedCapsule(fillColor: Color = .primary) -> some View {
        Capsule(style: .continuous)
            .fill(fillColor)
            .background {
                Capsule(style: .continuous)
                    .stroke(lineWidth: 4)
            }
    }
}

struct MathOperationsBlock_Previews: PreviewProvider {
    @StateObject static var viewModel = EquationInputViewModelImpl()
    
    static var previews: some View {
        MathKeyboardSectionButton(
            viewModel: viewModel,
            keyboardSection: .numbersAndGeneralOperations
        )
    }
}
