//
//  ShowSolutionButton.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 09.04.2023.
//

import SwiftUI

struct ShowSolutionButton<ViewModel: ExpressionModifierViewModel>: View {
    
    @ObservedObject var viewModel: ViewModel
    let action: () -> Void
    
    var body: some View {
        HStack {
            Text("Show solution")
            Image(systemName: "arrow.right")
        }
        .foregroundColor(.white)
        .font(.title2)
        .fontWeight(.semibold)
        .padding()
        .padding(.horizontal)
        .background {
            RoundedRectangle(cornerRadius: 20, style: .continuous)
                .fill(.pink)
        }
        .tint(.white)
        .opacity(viewModel.isReadyToSolve ? 1.0 : 0.0)
        .animation(
            .easeIn(duration: 0.2),
            value: viewModel.isReadyToSolve
        )
        .onTapGesture {
            action()
        }
    }
}

struct ShowSolutionButton_Previews: PreviewProvider {
    @State static var viewModel = EquationInputViewModelImpl()
    
    static var previews: some View {
        ShowSolutionButton(viewModel: viewModel, action: {})
    }
}
