//
//  AnswerText.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 26.03.2023.
//

import SwiftUI

struct AnswerText: View {
    let text: String
    
    var body: some View {
        Text(text)
            .font(.title2)
            .fontWeight(.bold)
            .foregroundColor(.pink)
    }
}

struct AnswerText_Previews: PreviewProvider {
    static var previews: some View {
        AnswerText(text: "F(x) = 10.245643")
    }
}
