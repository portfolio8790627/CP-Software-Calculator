//
//  EquationSolutionParametersRepresentation.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 23.02.2023.
//

import SwiftUI

struct EquationSolutionParametersRepresentation<ViewModel: EquationSolutionViewModel>: View {
    
    enum TextFieldInFocus {
        case epsilon
        case lowerBound
        case upperBound
    }
    
    //MARK: Properties
    
    @EnvironmentObject private var viewModel: ViewModel
    @FocusState private var textFieldFocus: TextFieldInFocus?
    
    @State private var lowerBoundValueText = String()
    @State private var upperBoundValueText = String()
    @State private var epsilonValueText = String()
    
    let equation: String
    
    //MARK: - Body

    var body: some View {
        VStack(alignment: .leading, spacing: 20) {
            ScrollView(.horizontal, showsIndicators: false) {
                HStack {
                    Text("F(x) =")
                    Text(equation)
                        .foregroundColor(.pink)
                }
                .font(.system(.title2, design: .rounded, weight: .medium))
                .padding(.top)
            }
            
            HStack {
                Text("Accuracy ε = ")
                ValueInputTextField(
                    title: "Num",
                    text: $epsilonValueText
                )
                .focused($textFieldFocus, equals: .epsilon)
                .onSubmit {
                    textFieldFocus = .lowerBound
                }
            }

            HStack {
                Text("Search from")
                ValueInputTextField(
                    title: "Low",
                    text: $lowerBoundValueText
                )
                .focused($textFieldFocus, equals: .lowerBound)
                .onSubmit {
                    textFieldFocus = .upperBound
                }
                
                Text("to")
                ValueInputTextField(
                    title: "High",
                    text: $upperBoundValueText
                )
                .focused($textFieldFocus, equals: .upperBound)
            }
        }
        .font(.headline)
        .onChange(of: lowerBoundValueText, perform: viewModel.lowerBoundInput.send)
        .onChange(of: upperBoundValueText, perform: viewModel.upperBoundInput.send)
        .onChange(of: epsilonValueText, perform: viewModel.epsilonInput.send)
    }
}

//MARK: - Preview

struct SolutionSearchParametersRepresentation_Previews: PreviewProvider {
    static var previews: some View {
        EquationSolutionParametersRepresentation<EquationSolutionViewModelImpl>(equation: "X^(4)-2×X-4")
            .environmentObject(
                EquationSolutionViewModelImpl(
                    expression: "X**4-2*X-4".expression,
                    solvingMethod: .dichotomy
                )
            )
    }
}
