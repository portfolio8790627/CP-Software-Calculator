//
//  DefiniteIntegralSolutionParametersRepresentation.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 08.04.2023.
//

import SwiftUI

struct DefiniteIntegralSolutionParametersRepresentation<ViewModel: DefiniteIntegralSolutionViewModel>: View {
    
    enum IntegralBound: String {
        case lower = "a"
        case upper = "b"
    }
    
    enum TextFieldInFocus {
        case lowerBound
        case upperBound
        case partitionsCount
        case epsilonValue
    }
    
    //MARK: Properties
    
    @EnvironmentObject private var viewModel: ViewModel
    
    @FocusState private var textFieldFocus: TextFieldInFocus?
    
    @State private var lowerBoundText = String()
    @State private var upperBoundText = String()
    @State private var partitionsCountText = String()
    @State private var epsilonValueText = String()
    
    let expressionString: String
    
    //MARK: - Body
    
    var body: some View {
        VStack(alignment: .leading) {
            ScrollView(.horizontal, showsIndicators: false) {
                DefiniteIntegralView(expressionString: expressionString)
                    .font(.system(.title2, design: .rounded, weight: .medium))
                    .padding(.bottom)
            }
            
            integralBoundInput(
                title: "Integral lower bound = ",
                bound: .lower
            )
            .focused($textFieldFocus, equals: .lowerBound)
            .onSubmit {
                textFieldFocus = .upperBound
            }
            
            integralBoundInput(
                title: "Integral upper bound = ",
                bound: .upper
            )
            .focused($textFieldFocus, equals: .upperBound)
            .onSubmit {
                textFieldFocus = .partitionsCount
            }
            
            HStack(spacing: 0.0) {
                Text("Partitions count = ")
                ValueInputTextField(
                    title: "Value",
                    text: $partitionsCountText
                )
                .focused($textFieldFocus, equals: .partitionsCount)
                .onSubmit {
                    textFieldFocus = .epsilonValue
                }
            }
            
            HStack(spacing: 0.0) {
                Text("Accuracy ε = ")
                ValueInputTextField(
                    title: "Num",
                    text: $epsilonValueText
                )
                .focused($textFieldFocus, equals: .epsilonValue)
            }
        }
        .font(.headline)
        .onChange(
            of: lowerBoundText,
            perform: viewModel.integralLowerBoundInput.send
        )
        .onChange(
            of: upperBoundText,
            perform: viewModel.integralUpperBoundInput.send
        )
        .onChange(
            of: partitionsCountText,
            perform: viewModel.partitionsCountInput.send
        )
        .onChange(
            of: epsilonValueText,
            perform: viewModel.epsilonInput.send
        )
    }
}

//MARK: - Local Views

private extension DefiniteIntegralSolutionParametersRepresentation {
    func integralBoundInput(title: String, bound: IntegralBound) -> some View {
        HStack(spacing: 0.0) {
            Text(title)
                .font(.headline)
            ValueInputTextField(
                title: bound.rawValue,
                text: bound == .lower ? $lowerBoundText : $upperBoundText
            )
        }
    }
}

//MARK: - Preview

struct DefiniteIntegralSolutionParametersRepresentation_Previews: PreviewProvider {
    static var previews: some View {
        DefiniteIntegralSolutionParametersRepresentation<DefiniteIntegralSolutionViewModelImpl>(expressionString: "sqrt(2*X+1)")
            .environmentObject(DefiniteIntegralSolutionViewModelImpl(expression: "sqrt(2*X+1)".expression, solvingMethod: .leftRectangles))
    }
}
