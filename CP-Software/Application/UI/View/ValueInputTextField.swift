//
//  ValueInputTextField.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 08.04.2023.
//

import SwiftUI

struct ValueInputTextField: View {
    
    //MARK: Properties
    
    let title: String
    @Binding var text: String
    
    //MARK: - Body
    
    var body: some View {
        TextField(title, text: $text)
            .foregroundColor(.pink)
            .multilineTextAlignment(.center)
            .frame(width: 80)
            .tint(.pink)
            .textFieldStyle(.roundedBorder)
            .keyboardType(.numbersAndPunctuation)
    }
}
