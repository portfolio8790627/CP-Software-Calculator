//
//  EquationSolutionAnswer.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 24.02.2023.
//

import SwiftUI

struct EquationSolutionAnswer: View {
    let answer: Double
    let functionValue: Double
    
    var body: some View {
        VStack(alignment: .leading) {
            Text("Answer:")
            Text("X = \(answer)")
            Text("F(x) = \(functionValue)")
        }
        .font(.title2)
        .fontWeight(.bold)
        .foregroundColor(.pink)
    }
}

struct Answer_Previews: PreviewProvider {
    static var previews: some View {
        EquationSolutionAnswer(answer: 1.14562, functionValue: 0.0452)
    }
}
