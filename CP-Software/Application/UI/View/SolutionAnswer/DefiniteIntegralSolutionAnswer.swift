//
//  DefiniteIntegralSolutionAnswer.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 10.04.2023.
//

import SwiftUI

struct DefiniteIntegralSolutionAnswer: View {
    let answer: Double
    
    var body: some View {
        VStack(alignment: .leading) {
            Text("Answer:")
            Text(String(answer))
        }
        .font(.title2)
        .fontWeight(.bold)
        .foregroundColor(.pink)
    }
}

struct DefiniteIntegralSolutionAnswer_Previews: PreviewProvider {
    static var previews: some View {
        DefiniteIntegralSolutionAnswer(answer: 1.257)
    }
}
