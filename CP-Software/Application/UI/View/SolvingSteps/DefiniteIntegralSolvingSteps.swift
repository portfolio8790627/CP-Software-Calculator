//
//  DefiniteIntegralSolvingSteps.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 09.04.2023.
//

import SwiftUI

struct DefiniteIntegralSolvingSteps: View {
    
    //MARK: Properties
    
    let solvingMethod: DefiniteIntegralSolvingMethod
    let iterationsInfo: [IterationInfo]
    
    //MARK: - Body
    
    var body: some View {
        switch solvingMethod {
        case .leftRectangles:
            if let leftRectanglesInfo = iterationsInfo as? [LeftRectanglesInfo] {
                ForEach(leftRectanglesInfo, id: \.iteration) { info in
                    LeftRecatnglesIteration(iterationInfo: info)
                }
            }
        case .trapezoidsMethod:
            if let trapezoidInfo = iterationsInfo as? [TrapezoidIterationInfo] {
                ForEach(trapezoidInfo, id: \.iteration) { info in
                    TrapezoidIteration(iterationInfo: info)
                }
            }
        case .simpsonMethod:
            if let simpsonsInfo = iterationsInfo as? [SimpsonIterationInfo] {
                ForEach(simpsonsInfo, id: \.iteration) { info in
                    SimpsonIteration(iterationInfo: info)
                }
            }
        }
    }
}
