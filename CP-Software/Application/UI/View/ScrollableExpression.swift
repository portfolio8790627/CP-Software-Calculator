//
//  ScrollableExpression.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 08.04.2023.
//

import SwiftUI

struct ScrollableExpression<ViewModel: ExpressionModifierViewModel, Content: View>: View {
    
    //MARK: Properties
    
    @ObservedObject var viewModel: ViewModel
    @Namespace private var equationTextId
    
    let textContent: Content
    
    //MARK: - Initialization
    
    init(viewModel: ViewModel, @ViewBuilder textContent: () -> Content) {
        self.viewModel = viewModel
        self.textContent = textContent()
    }
    
    //MARK: - Body
    
    var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            ScrollViewReader { scrollView in
                textContent
                .padding(.vertical)
                .id(equationTextId)
                .onChange(of: viewModel.expressionString) { _ in
                    scrollView.scrollTo(equationTextId, anchor: .trailing)
                }
            }
        }
        .padding()
    }
}

struct ScrollableText_Previews: PreviewProvider {
    @StateObject static var viewModel = EquationInputViewModelImpl()
    
    static var previews: some View {
        ScrollableExpression<EquationInputViewModelImpl, AnyView>(
            viewModel: viewModel) {
                AnyView(Text("Hello"))
            }
    }
}
