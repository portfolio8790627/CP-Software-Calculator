//
//  Calculator.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 08.04.2023.
//

import SwiftUI

struct Calculator<ViewModel: ExpressionModifierViewModel, InputContent, ButtonContent>: View where InputContent: View, ButtonContent: View {
    
    //MARK: Properties
    
    @ObservedObject var viewModel: ViewModel
    @Binding var isConfirmationPresented: Bool
    
    private let expressionInput: InputContent
    private let solutionButton: ButtonContent
    
    init(viewModel: ViewModel,
         isConfirmationPresented: Binding<Bool>,
         @ViewBuilder expressionInput: () -> InputContent,
         @ViewBuilder solutionButton: () -> ButtonContent) {
        self.viewModel = viewModel
        self._isConfirmationPresented = isConfirmationPresented
        self.expressionInput = expressionInput()
        self.solutionButton = solutionButton()
    }
    
    //MARK: - Body
    
    var body: some View {
        NavigationStack {
            GeometryReader { geometry in
                let size = geometry.size
                
                VStack {
                    expressionInput
                    Spacer()
                    solutionButton
                    MathKeyboard(viewModel: self.viewModel)
                        .padding(.bottom, 8)
                        .padding(.horizontal, 8)
                        .frame(maxHeight: size.height * 0.7)
                }
                .navigationTitle("Calculator")
                .navigationBarTitleDisplayMode(.inline)
                .frame(maxWidth: size.width, maxHeight: size.height)
            }
        }
    }
}
