//
//  DefiniteIntegralView.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 08.04.2023.
//

import SwiftUI

struct DefiniteIntegralView: View {
    
    enum IntegralBound: String {
        case lower = "a"
        case upper = "b"
    }
    
    //MARK: Properties
    
    let expressionString: String
    
    private var isExpressionEmpty: Bool {
        expressionString.isEmpty
    }
    
    //MARK: - Body
    
    var body: some View {
        HStack(spacing: 0) {
            integralSymbol
            
            Text("(").foregroundColor(.pink)
                .fontWeight(.semibold)
            Text(isExpressionEmpty ? "Input expression..." : expressionString)
                .foregroundColor(isExpressionEmpty ? .secondary : .primary)
            Text(") \(Text("dX").foregroundColor(.primary))")
                .foregroundColor(.pink)
                .fontWeight(.semibold)
        }
    }
}

//MARK: - Local Views

private extension DefiniteIntegralView {
    var integralSymbol: some View {
        VStack(spacing: 0) {
            integralBoundText(.upper)
            Text("∫")
                .font(.system(size: 50))
            integralBoundText(.lower)
        }
    }
    
    func integralBoundText(_ bound: IntegralBound) -> some View {
        Text(bound.rawValue)
            .foregroundColor(.secondary)
            .font(.subheadline)
            .fontWeight(.semibold)
    }
}

//MARK: - Preview

struct IntegralInput_Previews: PreviewProvider {
    static var previews: some View {
        DefiniteIntegralView(expressionString: "X+(2*X^(2))")
    }
}
