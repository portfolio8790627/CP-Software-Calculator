//
//  SolveButton.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 23.02.2023.
//

import SwiftUI

struct SolveButton: View {
    private let action: () -> Void
    
    init(action: @escaping () -> Void) {
        self.action = action
    }
    
    var body: some View {
        Text("Solve")
            .foregroundColor(.white)
            .font(.title2)
            .fontWeight(.semibold)
            .padding()
            .padding(.horizontal)
            .background {
                RoundedRectangle(cornerRadius: 20, style: .continuous)
                    .fill(.pink)
            }
            .onTapGesture {
                action()
            }
    }
}

struct SolveButton_Previews: PreviewProvider {
    static var previews: some View {
        SolveButton(action: {})
    }
}
