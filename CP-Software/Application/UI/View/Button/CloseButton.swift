//
//  CloseButton.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 11.04.2023.
//

import SwiftUI

struct CloseButton: View {
    let action: () -> Void
    
    var body: some View {
        Button("Close", action: action)
    }
}
