//
//  Double + Ext.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 24.02.2023.
//

extension Double {
    static var almostZero: Double { 0.001 }
    
    var isGreaterThanZero: Bool {
        self > 0
    }
    
    var doNotAllowZero: Double {
        self != 0 ? self : Self.almostZero
    }
}
