//
//  Publisher + Ext.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 08.04.2023.
//

import Combine

extension Publisher where Output == String, Failure == Never {
    func mapInput<NumberType: LosslessStringConvertible>(to type: NumberType.Type) -> AnyPublisher<NumberType?, Never> {
        map { NumberType($0) ?? nil }
            .eraseToAnyPublisher()
    }
    
    func mapToNumberAndSend<NumberType: LosslessStringConvertible>(numberType: NumberType.Type, to subject: any Subject<NumberType?, Never>, stroreIn cancellables: inout Set<AnyCancellable>) {
        mapInput(to: numberType)
            .sink { subject.send($0) }
            .store(in: &cancellables)
    }
}
