//
//  Int + Ext.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 09.04.2023.
//

import Foundation

extension Int {
    var isGreaterThanZero: Bool {
        self > 0
    }
}
