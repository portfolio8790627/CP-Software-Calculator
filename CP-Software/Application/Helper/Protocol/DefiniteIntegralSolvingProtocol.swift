//
//  DefiniteIntegralSolvingProtocol.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 08.04.2023.
//

import Foundation

protocol DefiniteIntegralSolvingProtocol {
    var p: Int { get }
    
    func solve(range: ClosedRange<Double>,
               partitionsCount: Int,
               function: (_ x: Double) -> Double) async -> (answer: Double, iterationsInfo: [IterationInfo])
}
