//
//  EquationSolvingProtocol.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 09.04.2023.
//

protocol EquationSolvingProtocol {
    func solve() async -> (answer: Double?, iterationsInfo: [IterationInfo])
}
