//
//  FunctionInterpolationSolvingService.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 11.04.2023.
//

import Foundation

protocol FunctionInterpolationSolvingService {
    var xValues: [Double] { get set }
    var functionValues: [Double] { get set }
    var targetX: Double { get set }
    func solve() async -> Double?
}
