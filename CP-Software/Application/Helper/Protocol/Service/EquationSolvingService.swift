//
//  EquationSolvingService.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 08.04.2023.
//

import Foundation

protocol EquationSolvingService {
    var expression: NSExpression { get set }
    var range: ClosedRange<Double> { get set }
    var epsilon: Double { get set }
    func solve(by solvingMethod: EquationSolvingMethod) async -> (answer: Double?, iterationsInfo: [IterationInfo])
    func expressionFunction(_ x: Double) -> Double
}
