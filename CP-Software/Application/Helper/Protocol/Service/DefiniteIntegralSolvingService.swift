//
//  DefiniteIntegralSolvingService.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 08.04.2023.
//

import Foundation

protocol DefiniteIntegralSolvingService {
    var expression: NSExpression { get set }
    var range: ClosedRange<Double> { get set }
    var partitionsCount: Int { get set }
    var epsilon: Double { get set }
    func solve(by solvingMethod: DefiniteIntegralSolvingMethod) async -> (answer: Double, iterationsInfo: [IterationInfo])
    func approximationFunction(previous: Double, next: Double, p: Int) -> Double
}

extension DefiniteIntegralSolvingService {
    func approximationFunction(previous: Double, next: Double, p: Int) -> Double {
        abs(next - previous) / (pow(2, Double(p)) - 1)
    }
}
