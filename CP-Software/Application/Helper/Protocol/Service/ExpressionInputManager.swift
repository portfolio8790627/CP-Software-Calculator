//
//  ExpressionInputManager.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 01.04.2023.
//

import Combine

protocol ExpressionInputManager: ExpressionModifier {
    var equationSubject: CurrentValueSubject<String, Never> { get }
    var equationComponents: [String] { get }
    var equationExpressionComponents: [String] { get }
    
    var isEquationEmpty: Bool { get }
    var areBracketsBalanced: Bool { get }
    var isLastBasicOperation: Bool { get }
    var isLastDot: Bool { get }
}

