//
//  ExpressionModifier.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 01.04.2023.
//

protocol ExpressionModifier {
    func addNumber(_ number: Int)
    func addBasicOperation(_ operation: BasicMathOperation)
    func addAdvancedOperation(_ operation: AdvancedMathOperation)
    func addAuxiliaryOperation(_ operation: AuxiliaryMathOperation)
    func removeLastOperation()
    func clearAll()
}
