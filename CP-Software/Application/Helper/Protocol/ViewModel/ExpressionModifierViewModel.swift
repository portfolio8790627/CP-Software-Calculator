//
//  ExpressionModifierViewModel.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 08.04.2023.
//

import Foundation
import Combine

protocol ExpressionModifierViewModel: ObservableObject, ExpressionModifier {
    var expressionString: String { get }
    var isExpressionEmpty: Bool { get }
    var isReadyToSolve: Bool { get }
    var currentKeyboardSection: CurrentValueSubject<MathKeyboardSection, Never> { get }
    var expression: NSExpression { get }
}
