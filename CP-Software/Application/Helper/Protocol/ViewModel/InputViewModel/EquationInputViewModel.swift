//
//  EquationInputViewModel.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 01.04.2023.
//

import Foundation
import Combine

protocol EquationInputViewModel: ExpressionModifierViewModel {
    var solvingMethods: [EquationSolvingMethod] { get }
}

