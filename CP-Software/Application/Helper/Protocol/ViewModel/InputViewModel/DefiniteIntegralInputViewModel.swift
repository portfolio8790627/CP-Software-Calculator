//
//  DefiniteIntegralInputViewModel.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 08.04.2023.
//

import Foundation
import Combine

protocol DefiniteIntegralInputViewModel: ExpressionModifierViewModel {
    var solvingMethods: [DefiniteIntegralSolvingMethod] { get }
}
