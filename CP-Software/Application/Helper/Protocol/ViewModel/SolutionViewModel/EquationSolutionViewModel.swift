//
//  EquationSolutionViewModel.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 09.04.2023.
//

import Foundation
import Combine

protocol EquationSolutionViewModel: ObservableObject, ExpressionSolver, PreviousInputChecker {
    var lowerBoundInput: CurrentValueSubject<String, Never> { get }
    var upperBoundInput: CurrentValueSubject<String, Never> { get }
    var solvingMethod: EquationSolvingMethod { get }
    func functionValue(_ x: Double) -> Double
}
