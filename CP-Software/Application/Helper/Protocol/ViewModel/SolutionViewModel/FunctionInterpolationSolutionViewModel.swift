//
//  FunctionInterpolationSolutionViewModel.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 11.04.2023.
//

import Combine

protocol FunctionInterpolationSolutionViewModel: ObservableObject, PreviousInputChecker {
    var firstXInput: CurrentValueSubject<String, Never> { get }
    var secondXInput: CurrentValueSubject<String, Never> { get }
    
    var firstFunctionValueInput: CurrentValueSubject<String, Never> { get }
    var secondFunctionValueInput: CurrentValueSubject<String, Never> { get }
    
    var targetXValueInput: CurrentValueSubject<String, Never> { get }
    
    var isSolveButtonEnable: Bool { get }
    var answer: Double? { get }
    
    func solve() async
}
