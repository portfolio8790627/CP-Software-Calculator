//
//  DefiniteIntegralSolutionViewModel.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 08.04.2023.
//

import Foundation
import Combine

protocol DefiniteIntegralSolutionViewModel: ObservableObject, ExpressionSolver, PreviousInputChecker {
    var solvingMethod: DefiniteIntegralSolvingMethod { get }
    var integralLowerBoundInput: CurrentValueSubject<String, Never> { get }
    var integralUpperBoundInput: CurrentValueSubject<String, Never> { get }
    var partitionsCountInput: CurrentValueSubject<String, Never> { get }
}
