//
//  ExpressionSolver.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 09.04.2023.
//

import Foundation
import Combine

protocol ExpressionSolver {
    var expression: NSExpression { get }
    var solvingMethodTitle: String { get }
    var isSolveButtonEnable: Bool { get }
    var epsilonInput: CurrentValueSubject<String, Never> { get }
    var answer: Double? { get }
    var iterationsInfo: [IterationInfo] { get }
    func solve() async
}
