//
//  PreviousInputChecker.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 11.04.2023.
//

protocol PreviousInputChecker {
    var arePreviousValuesNotEntered: Bool { get }
}
