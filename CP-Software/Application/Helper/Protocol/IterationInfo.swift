//
//  IterationInfo.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 01.04.2023.
//

protocol IterationInfo {
    var iteration: Int { get }
}
