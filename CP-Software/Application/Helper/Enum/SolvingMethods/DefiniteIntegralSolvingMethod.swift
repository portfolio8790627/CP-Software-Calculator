//
//  DefiniteIntegralSolvingMethod.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 08.04.2023.
//

enum DefiniteIntegralSolvingMethod: String, CaseIterable {
    case leftRectangles = "Left Rectangles"
    case trapezoidsMethod = "Trapezoids Method"
    case simpsonMethod = "Simpson's Method"
}
