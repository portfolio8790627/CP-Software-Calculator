//
//  EquationSolvingMethod.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 21.02.2023.
//

enum EquationSolvingMethod: String, CaseIterable {
    case dichotomy = "Dichotomy Method"
    case simpleIterations = "Iterations Method"
    case newton = "Newton's Method"
}
