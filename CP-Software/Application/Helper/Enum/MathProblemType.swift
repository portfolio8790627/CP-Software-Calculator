//
//  MathProblemType.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 08.04.2023.
//

enum MathProblemType: CaseIterable {
    case nonlinearEquations
    case definiteIntegrals
    case functionInterpolation
    
    var title: String {
        switch self {
        case .nonlinearEquations:
            return "Nonlinear Equations"
        case .definiteIntegrals:
            return "Definite Integrals"
        case .functionInterpolation:
            return "Function Interpolation"
        }
    }
    
    var description: String {
        switch self {
        case .nonlinearEquations:
            return "Solving nonlinear equations by Dichotomy, Simple Iterations and Newton methods"
        case .definiteIntegrals:
            return "Solving definite integrals by methods of Left Rectangles, Trapezoids and Parabolas (Simpson's method)"
        case .functionInterpolation:
            return "Solving the function interpolation problem"
        }
    }
}

extension MathProblemType: Identifiable {
    var id: Self { self }
}
