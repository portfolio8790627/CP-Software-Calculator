//
//  DefiniteIntegralSolvingServiceImpl.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 08.04.2023.
//

import Foundation

final class DefiniteIntegralSolvingServiceImpl: DefiniteIntegralSolvingService {
    
    typealias SolutionResult = (answer: Double, iterationsInfo: [IterationInfo])
    
    //MARK: Properties
    
    var expression: NSExpression
    var range: ClosedRange<Double>
    var partitionsCount: Int
    var epsilon: Double
    
    //MARK: - Initialization
    
    init(expression: NSExpression,
         range: ClosedRange<Double>,
         partitionsCount: Int,
         epsilon: Double) {
        self.expression = expression
        self.range = range
        self.partitionsCount = partitionsCount
        self.epsilon = epsilon
    }
    
    //MARK: - Methods
    
    func solve(by solvingMethod: DefiniteIntegralSolvingMethod) async -> SolutionResult {
        switch solvingMethod {
        case .leftRectangles:
            return await solveByLeftRectanglesMethod()
        case .trapezoidsMethod:
            return await solveByTrapezoidMethod()
        case .simpsonMethod:
            return await solveBySimpsonMethod()
        }
    }
}

//MARK: - Private methods

private extension DefiniteIntegralSolvingServiceImpl {
    func solveByLeftRectanglesMethod() async -> SolutionResult {
        let method = LeftRectanglesMethod()
        let result = await method.solve(
            range: range,
            partitionsCount: partitionsCount,
            function: expression.expressionFunction
        )
        return await increaseAccuracy(ofMethod: method, previousResult: result)
    }
    
    func solveByTrapezoidMethod() async -> SolutionResult {
        let method = TrapezoidMethod()
        let result = await method.solve(
            range: range,
            partitionsCount: partitionsCount,
            function: expression.expressionFunction
        )
        return await increaseAccuracy(ofMethod: method, previousResult: result)
    }
    
    func solveBySimpsonMethod() async -> SolutionResult {
        let method = SimpsonMethod()
        let result = await method.solve(
            range: range,
            partitionsCount: partitionsCount,
            function: expression.expressionFunction
        )
        return await increaseAccuracy(ofMethod: method, previousResult: result)
    }
    
    func increaseAccuracy(ofMethod method: DefiniteIntegralSolvingProtocol,
                          previousResult: SolutionResult) async -> SolutionResult {
        var newPartitionsCount = partitionsCount * 2
        
        var previous = previousResult
        var next = await method.solve(range: range, partitionsCount: newPartitionsCount, function: expression.expressionFunction)
        
        while approximationFunction(previous: previous.answer, next: next.answer, p: method.p) > epsilon {
            newPartitionsCount *= 2
            previous = next
            
            next = await method.solve(
                range: range,
                partitionsCount: newPartitionsCount * 2,
                function: expression.expressionFunction
            )
        }
        
        return next
    }
}
