//
//  TrapezoidMethod.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 10.04.2023.
//

struct TrapezoidMethod: DefiniteIntegralSolvingProtocol {
    
    typealias Function = (Double) -> Double
    
    //MARK: Properties
    
    let p: Int = 2
    
    //MARK: - Methods
    
    func solve(range: ClosedRange<Double>,
               partitionsCount: Int,
               function: Function) async -> (answer: Double, iterationsInfo: [IterationInfo]) {
        var iterationsInfo: [TrapezoidIterationInfo] = []
        
        let trapezoidWidth = (range.upperBound - range.lowerBound) / Double(partitionsCount)
        var sum = 0.0
        
        var iteration = 1
        
        for i in stride(from: range.lowerBound + trapezoidWidth, to: range.upperBound, by: trapezoidWidth) {
            let functionValue = function(i != 0 ? i : Double.almostZero)
            sum += functionValue
            
            iterationsInfo.append(
                TrapezoidIterationInfo(iteration: iteration, x: i, functionValue: functionValue)
            )
            
            iteration += 1
        }
        
        let coefficient = calculateCoefficient(a: range.lowerBound, b: range.upperBound, function: function)
        
        return (trapezoidWidth * (coefficient + sum), iterationsInfo)
    }
}

//MARK: - Private methods

private extension TrapezoidMethod {
    func calculateCoefficient(a: Double, b: Double, function: Function) -> Double {
        return (function(a.doNotAllowZero) + function(b.doNotAllowZero)) / 2
    }
}
