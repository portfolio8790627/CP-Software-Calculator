//
//  LeftRectanglesMethod.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 26.03.2023.
//

struct LeftRectanglesMethod: DefiniteIntegralSolvingProtocol {
    
    //MARK: Properties
    
    let p: Int = 1
    
    //MARK: - Methods
    
    func solve(range: ClosedRange<Double>,
               partitionsCount: Int,
               function: (_ x: Double) -> Double) async -> (answer: Double, iterationsInfo: [IterationInfo]) {
        var iterationsInfo: [LeftRectanglesInfo] = []
        
        let rectangleWidth = (range.upperBound - range.lowerBound) / Double(partitionsCount)
        var sum = 0.0
        
        var iteration = 1
        
        for i in stride(from: range.lowerBound, to: range.upperBound, by: rectangleWidth) {
            let functionValue = function(i != 0 ? i : Double.almostZero)
            sum += functionValue
            
            iterationsInfo.append(
                LeftRectanglesInfo(iteration: iteration, x: i, functionValue: functionValue)
            )
            
            iteration += 1
        }
        
        return (rectangleWidth * sum, iterationsInfo)
    }
}
