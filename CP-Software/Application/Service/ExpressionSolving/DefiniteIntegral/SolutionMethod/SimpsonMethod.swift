//
//  SimpsonMethod.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 10.04.2023.
//

struct SimpsonMethod: DefiniteIntegralSolvingProtocol {
    
    //MARK: Properties
    
    let p: Int = 4
    
    //MARK: - Methods
    
    func solve(range: ClosedRange<Double>, partitionsCount: Int, function: (Double) -> Double) async -> (answer: Double, iterationsInfo: [IterationInfo]) {
        let dx = (range.upperBound - range.lowerBound) / Double(partitionsCount)
        var sum = 0.0
        
        var iterationsInfo = [SimpsonIterationInfo]()
        var iteration = 1
        
        for i in stride(from: 0, to: partitionsCount, by: 2) {
            let x1 = range.lowerBound + Double(i) * dx
            let x2 = x1 + dx
            let x3 = x2 + dx
            
            let functionValue1 = function(x1.doNotAllowZero)
            let functionValue2 = function(x2.doNotAllowZero)
            let functionValue3 = function(x3.doNotAllowZero)
            
            let area = (dx / 3.0) * (functionValue1 + 4.0 * functionValue2 + functionValue3)
            sum += area
            
            iterationsInfo.append(
                SimpsonIterationInfo(
                    iteration: iteration,
                    x1: x1,
                    x2: x2,
                    x3: x3
                )
            )
            
            iteration += 1
        }
        
        return (sum, iterationsInfo)
    }
    
    
}
