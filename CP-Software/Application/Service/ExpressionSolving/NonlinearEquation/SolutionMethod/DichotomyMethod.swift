//
//  DichotomyMethod.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 24.02.2023.
//

import Foundation

struct DichotomyMethod: EquationSolvingProtocol {

    //MARK: Properties
    
    var equation: NSExpression
    var range: ClosedRange<Double>
    var epsilon: Double
    
    //MARK: - Methods
    
    func solve() async -> (answer: Double?, iterationsInfo: [IterationInfo]) {
        guard epsilon > 0 else { return (nil, []) }
        
        var iteration = 0
        var iterationsInfo: [DichotomyIterationInfo] = []
        
        var newA = range.lowerBound != 0 ? range.lowerBound : Double.almostZero
        var newB = range.upperBound != 0 ? range.upperBound : Double.almostZero
        
        var result: Double?

        while abs(equation.expressionFunction(newB) - equation.expressionFunction(newA)) > epsilon {
            iteration += 1
            
            let midValue = (newA + newB) / 2
            
            iterationsInfo.append(
                DichotomyIterationInfo(
                    iteration: iteration,
                    a: newA, b: newB,
                    x: midValue
                )
            )
            
            if midValue == 0 || abs(equation.expressionFunction(midValue)) < epsilon {
                result = midValue
                break
            } else if equation.expressionFunction(newA) * equation.expressionFunction(midValue) < 0 {
                newB = midValue
            } else {
                newA = midValue
            }
        }
        
        return (result, iterationsInfo)
    }
}
