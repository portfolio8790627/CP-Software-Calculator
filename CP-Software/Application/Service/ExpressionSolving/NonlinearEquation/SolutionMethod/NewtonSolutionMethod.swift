//
//  NewtonSolutionMethod.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 24.02.2023.
//

import Foundation

struct NewtonSolutionMethod: EquationSolvingProtocol {
    
    //MARK: Properties
    
    var equation: NSExpression
    var range: ClosedRange<Double>
    var epsilon: Double
    
    //MARK: - Methods
    
    func solve() async -> (answer: Double?, iterationsInfo: [IterationInfo]) {
        guard epsilon > 0 else { return (nil, []) }
        
        var iterationsInfo: [NewtonIterationInfo] = []
        var result: Double?
        
        var x = chooseStartPoint(between: range.lowerBound, and: range.upperBound)
        
        var iteration = 1
        var derivativeX = equation.derivative(at: x)
        let secondDerivativeX = equation.derivativeSecond(at: x)
        
        iterationsInfo.append(
            NewtonIterationInfo(
                iteration: iteration,
                derivative: derivativeX,
                secondDerivative: secondDerivativeX,
                x: x
            )
        )
        
        while abs(equation.expressionFunction(x)) > epsilon {
            iteration += 1
            
            let nextX = x - (equation.expressionFunction(x) / derivativeX)
            derivativeX = equation.derivative(at: nextX)
            
            iterationsInfo.append(
                NewtonIterationInfo(
                    iteration: iteration,
                    derivative: derivativeX,
                    secondDerivative: nil,
                    x: nextX
                )
            )
            
            guard nextX >= range.lowerBound, nextX <= range.upperBound else {
                return (nil, iterationsInfo)
            }
            
            if abs(equation.expressionFunction(nextX)) <= epsilon {
                result = nextX
                break
            } else {
                x = nextX
            }
        }
        
        return (result, iterationsInfo)
    }
}

//MARK: - Private methods

private extension NewtonSolutionMethod {
    func chooseStartPoint(between a: Double, and b: Double) -> Double {
        let newA = a != 0 ? a : Double.almostZero
        let newB = b != 0 ? b : Double.almostZero
        
        if equation.expressionFunction(newA) * equation.derivativeSecond(at: newA) > 0 {
            return newA != Double.almostZero ? newA : newB
        } else {
            return newB
        }
    }
}
