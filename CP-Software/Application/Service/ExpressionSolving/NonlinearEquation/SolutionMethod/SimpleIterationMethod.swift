//
//  SimpleIterationMethod.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 24.02.2023.
//

import Foundation

struct SimpleIterationMethod: EquationSolvingProtocol {
    
    //MARK: Properties
    
    var equation: NSExpression
    var range: ClosedRange<Double>
    var epsilon: Double
    
    //MARK: - Methods
    
    func solve() async -> (answer: Double?, iterationsInfo: [IterationInfo]) {
        guard epsilon > 0 else { return (nil, []) }
        
        var iteration = 1
        var iterationsInfo: [SimpleIterationInfo] = []
        var result: Double?
        
        var x = chooseStartPoint(between: range.lowerBound, and: range.upperBound)
        
        let derivative = equation.derivative(at: x)
        let m = 1.01 * derivative
        
        var nextX = fi(x: x, m: m)
        
        iterationsInfo.append(
            SimpleIterationInfo(
                iteration: iteration,
                x: x,
                m: m,
                functionDerivative: derivative
            )
        )
        
        while abs(nextX - x) > epsilon {
            iteration += 1
            let nextIterationX = fi(x: nextX, m: m)

            iterationsInfo.append(
                SimpleIterationInfo(
                    iteration: iteration,
                    x: nextIterationX,
                    m: nil, functionDerivative: nil
                )
            )
            
            guard nextIterationX >= range.lowerBound, nextIterationX <= range.upperBound else {
                return (nil, iterationsInfo)
            }
            if abs(nextIterationX - nextX) <= epsilon {
                result = nextIterationX
                break
            } else {
                x = nextX
                nextX = nextIterationX
            }
        }
        
        return (result, iterationsInfo)
    }
}

//MARK: - Private methods

private extension SimpleIterationMethod {
    func chooseStartPoint(between a: Double, and b: Double) -> Double {
        if a < b, a < 0, abs(a) >= abs(b) {
            return a != 0 ? a : Double.almostZero
        } else {
            return b != 0 ? b : Double.almostZero
        }
    }
    
    func fi(x: Double, m: Double) -> Double {
        x - equation.expressionFunction(x) / m
    }
}
