//
//  EquationSolvingServiceImpl.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 24.02.2023.
//

import Foundation

final class EquationSolvingServiceImpl: EquationSolvingService {
    
    typealias SolutionResult = (answer: Double?, iterationsInfo: [IterationInfo])
    
    //MARK: Properties
    
    var expression: NSExpression
    var range: ClosedRange<Double>
    var epsilon: Double
    
    //MARK: - Initialization
    
    init(expression: NSExpression, range: ClosedRange<Double>, epsilon: Double) {
        self.expression = expression
        self.range = range
        self.epsilon = epsilon
    }
    
    //MARK: - Methods
    
    func solve(by solvingMethod: EquationSolvingMethod) async -> SolutionResult {
        switch solvingMethod {
        case .dichotomy:
            return await solveByDichotomyMethod()
        case .simpleIterations:
            return await solveBySimpleIterations()
        case .newton:
            return await solveByNewtonMethod()
        }
    }
    
    func expressionFunction(_ x: Double) -> Double {
        expression.expressionFunction(x)
    }
}

//MARK: - Private methods

private extension EquationSolvingServiceImpl {
    func solveByDichotomyMethod() async -> SolutionResult {
        let method = DichotomyMethod(
            equation: self.expression,
            range: self.range,
            epsilon: self.epsilon
        )
        return await method.solve()
    }
    
    func solveBySimpleIterations() async -> SolutionResult {
        let method = SimpleIterationMethod(
            equation: self.expression,
            range: self.range,
            epsilon: self.epsilon
        )
        return await method.solve()
    }
    
    func solveByNewtonMethod() async -> SolutionResult {
        let method = NewtonSolutionMethod(
            equation: self.expression,
            range: self.range,
            epsilon: self.epsilon
        )
        return await method.solve()
    }
}
