//
//  FunctionInterpolationSolvingServiceImpl.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 11.04.2023.
//

import Foundation

struct FunctionInterpolationSolvingServiceImpl: FunctionInterpolationSolvingService {
    
    //MARK: Properties
    
    var xValues: [Double]
    var functionValues: [Double]
    var targetX: Double
    
    //MARK: - Methods
    
    func solve() async -> Double? {
        lagrangeInterpolation(x: xValues, y: functionValues, xValue: targetX)
    }
}

//MARK: - Private methods

private extension FunctionInterpolationSolvingServiceImpl {
    func lagrangeInterpolation(x: [Double], y: [Double], xValue: Double) -> Double? {
        guard !x.isEmpty, !y.isEmpty else { return nil }
        guard x.count == y.count else { return nil }
        
        let n = x.count
        var result = 0.0
        
        for i in 0..<n {
            var term = 1.0
            for j in 0..<n {
                if i != j {
                    term *= (xValue - x[j]) / (x[i] - x[j])
                }
            }
            result += y[i] * term
        }
        
        return result
    }
}
