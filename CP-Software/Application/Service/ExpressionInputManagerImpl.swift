//
//  ExpressionInputManagerImpl.swift
//  CP-Software
//
//  Created by Малиль Дугулюбгов on 19.03.2023.
//

import Foundation
import Combine

final class ExpressionInputManagerImpl: ExpressionInputManager {
    
    //MARK: Properties
    
    var equationSubject: CurrentValueSubject<String, Never> = .init("")
    
    var equationComponents: [String] = []
    var equationExpressionComponents: [String] = []
    
    private var allowsDot = true
    private var isEndEnteringCustomOperation = true
    private var leftedCustomOperation: String?
    
    private var equation: String {
        equationComponents.joined()
    }
    
    //MARK: - Input Checks
    
    var isEquationEmpty: Bool { equationComponents.isEmpty }
    
    var isLastNumber: Bool { Double(equationComponents.last ?? "") != nil }
    
    var isLastDot: Bool { equationComponents.last == "." }
    
    var isLastOpeningBracket: Bool { isLastContains("(") }
    var isLastClosingBracket: Bool { isLastContains(")") }
    var areBracketsBalanced: Bool { equation.areBracketsBalanced() }
    
    var allowsSubtracton: Bool {
        isEquationEmpty ||
        isLastNumber ||
        isLastClosingBracket ||
        isLastOpeningBracket ||
        isLastX ||
        isLastExponent
    }
    
    var isLastExponent: Bool { equationComponents.last == "e" }
    
    var isLastX: Bool { equationComponents.last == AuxiliaryMathOperation.x.title }
    
    var isLastBasicOperation: Bool {
        guard let last = equationComponents.last else { return false }
        
        switch last {
        case BasicMathOperation.add.title: return true
        case BasicMathOperation.sub.title: return true
        case BasicMathOperation.mul.title: return true
        case BasicMathOperation.div.title: return true
        default: return false
        }
    }
    
    var allowsDigit: Bool {
        isLastNumber || allowsVariable || isLastDot
    }
    
    var allowsVariable: Bool {
        isEquationEmpty || isLastOpeningBracket || isLastBasicOperation
    }
    
    var allowsOpeningBracket: Bool {
        isEquationEmpty || isLastOpeningBracket || isLastBasicOperation
    }
    
    var allowsClosingBracket: Bool {
        !areBracketsBalanced && (isLastNumber || isLastX || isLastExponent || isLastClosingBracket)
    }
    
    //MARK: - Methods
    
    func addNumber(_ number: Int) {
        guard allowsDigit else { return }
        let strNumber = String(number)
        supplementEquation(withEquationComponents: strNumber, withEquationExpression: strNumber)
    }
    
    func addBasicOperation(_ operation: BasicMathOperation) {
        guard allowsBasicOperation(operation) else { return }
        supplementEquation(withEquationComponents: operation.title, withEquationExpression: operation.rawValue)
        allowsDot = true
    }
    
    func addAdvancedOperation(_ operation: AdvancedMathOperation) {
        guard allowsAdvancedOperation(operation) else { return }
        
        switch operation {
        case .pow:
            supplementEquation(
                withEquationComponents: "^(",
                withEquationExpression: "**("
            )
        case .sqrt:
            supplementEquation(
                withEquationComponents: "sqrt(",
                withEquationExpression: "sqrt("
            )
        case .e:
            supplementEquation(
                withEquationComponents: "e",
                withEquationExpression: "exp(1)"
            )
        case .exp:
            supplementEquation(
                withEquationComponents: "exp(",
                withEquationExpression: "exp("
            )
        case .lg:
            supplementEquation(
                withEquationComponents: "lg(",
                withEquationExpression: "log("
            )
        case .ln:
            supplementEquation(
                withEquationComponents: "ln(",
                withEquationExpression: "ln("
            )
        case let customOperation where customOperation.isCustomExpressionFunction:
            supplementEquation(
                withEquationComponents: "\(customOperation.rawValue)(",
                withEquationExpression: "function("
            )
            leftedCustomOperation = operation.rawValue
            isEndEnteringCustomOperation = false
        default:
            break
        }
    }
    
    func addAuxiliaryOperation(_ operation: AuxiliaryMathOperation) {
        switch operation {
        case .dot:
            guard isLastNumber, allowsDot else { return }
            supplementEquation(
                withEquationComponents: ".",
                withEquationExpression: "."
            )
            allowsDot = false
        case .x:
            guard allowsVariable else { return }
            let x = "\(AuxiliaryMathOperation.x.title)"
            supplementEquation(
                withEquationComponents: x,
                withEquationExpression: x
            )
        case .roundBracket:
            if allowsOpeningBracket {
                supplementEquation(
                    withEquationComponents: "(",
                    withEquationExpression: "("
                )
            } else {
                addClosingBracket()
            }
        }
    }
    
    func removeLastOperation() {
        guard !equationComponents.isEmpty, !equationExpressionComponents.isEmpty else { return }
        equationComponents.removeLast()
        equationSubject.send(equation)
        removeExpressionLastOperation()
    }
    
    func clearAll() {
        equationComponents.removeAll()
        equationExpressionComponents.removeAll()
        equationSubject.send(equation)
        setDefaultValues()
    }
}

//MARK: - Private methods

private extension ExpressionInputManagerImpl {
    func isLastContains(_ otherString: String) -> Bool {
        guard let last = equationComponents.last else { return false }
        return last.contains(otherString)
    }
    
    func allowsAdvancedOperation(_ operation: AdvancedMathOperation) -> Bool {
        switch operation {
        case .pow:
            return isLastNumber || isLastX || isLastClosingBracket
        case .sqrt:
            return isEquationEmpty || isLastBasicOperation || isLastOpeningBracket
        default:
            return allowsVariable
        }
    }
    
    func supplementEquation(withEquationComponents component: String, withEquationExpression expression: String) {
        equationComponents.append(component)
        equationExpressionComponents.append(expression)
        equationSubject.send(equation)
    }
    
    func addClosingBracket() {
        guard allowsClosingBracket else { return }
        
        if let leftedCustomOperation, isEndEnteringCustomOperation == false {
            endCustomOperationWithBracket(leftedOperation: leftedCustomOperation)
        } else {
            supplementEquation(withEquationComponents: ")", withEquationExpression: ")")
        }
    }
    
    func removeExpressionLastOperation() {
        let expressionLast = equationExpressionComponents.removeLast()
        
        if expressionLast == ".", allowsDot == false {
            allowsDot = true
        }
        
        if expressionLast.contains(",") {
            isEndEnteringCustomOperation = false
        }
        
        for operation in AdvancedMathOperation.allCases where operation.isCustomExpressionFunction {
            if expressionLast.contains(operation.rawValue) {
                leftedCustomOperation = operation.rawValue
            } else if expressionLast.contains("function(") {
                isEndEnteringCustomOperation = true
            }
        }
    }
    
    func setDefaultValues() {
        allowsDot = true
        isEndEnteringCustomOperation = true
        leftedCustomOperation = nil
    }
    
    func endCustomOperationWithBracket(leftedOperation: String) {
        let customOperationBlock = extractCustomOperationBlock(of: leftedOperation)
        
        guard customOperationBlock.appending(")").areBracketsBalanced() else {
            supplementEquation(withEquationComponents: ")", withEquationExpression: ")")
            return
        }
        
        supplementEquation(
            withEquationComponents: ")",
            withEquationExpression: ", '\(leftedOperation)')"
        )
        
        isEndEnteringCustomOperation = true
    }
    
    func allowsBasicOperation(_ operation: BasicMathOperation) -> Bool {
        switch operation {
        case .sub:
            return allowsSubtracton
        case .add, .mul, .div:
            return isLastNumber || isLastX || isLastExponent || isLastClosingBracket
        }
    }
    
    func extractCustomOperationBlock(of leftedOperation: String) -> String {
        var customBlock = [String]()
        for operation in equationComponents.reversed() {
            customBlock.insert(operation, at: 0)
            if operation == leftedOperation + "(" { break }
        }
        return customBlock.joined()
    }
}
