# CP-Software-Calculator

![Fotoram io](https://user-images.githubusercontent.com/81229461/224536647-21671194-09be-4a2f-a99e-f1559ecd1273.png)

The application is an educational project created for a university project on the subject "Software of computational processes". This is a calculator for solving mathematical problems of a certain type, allowing you to use various methods to find a solution

![IMG_4049](https://user-images.githubusercontent.com/81229461/231578006-260ceb3c-7f04-480b-aa80-69ac489a5569.png)

## Preview
![IMG_4048](https://user-images.githubusercontent.com/81229461/231578053-21e7426d-fb6b-4025-84f2-30eb400830c7.png)

## Solution methods
There are several types of mathematical problems available for selection in the application, for each of which it is possible to choose several methods of solution

### Solving Equations
- The Dichotomy Method
- Simple Iterations method
- Newton's method

### Solving Definite Integrals
- Left Rectangles method
- Trapezoid method
- Simpson's method

### Function Linear Interpolation
- Lagrange polynomials

![IMG_4050](https://user-images.githubusercontent.com/81229461/231579102-fa69a0d4-54f1-43a6-a8f4-adb0705d0c36.png)
